
const axios = require("axios").default;
// Замените 'YOUR_BOT_TOKEN' на токен вашего бота
const BOT_TOKEN = '8171305709:AAHl4Z2jUvdfqyXI1sICkbOMlZpKjcoYmSM';
const TELEGRAM_API_URL = `https://api.telegram.org/bot${BOT_TOKEN}/`;


exports.handler = async (event) => {
    console.log("Received an update from Telegram!", event.body);
    const body = JSON.parse(event.body);
    await handleUpdate(body)
    return { statusCode: 200 };
};


// Функция для отправки запросов к Telegram API
async function sendTelegramMessage(chatId, text) {
  const url = `${TELEGRAM_API_URL}sendMessage`;
  try {
    const response = await axios.post(url, {
      chat_id: chatId,
      text: text,
    });
    console.log('Message sent:', response.data);
  } catch (error) {
    console.error('Error sending message:', error.response ? error.response.data : error.message);
  }
}

// Функция для преобразования ссылки
function transformInstagramLink(link) {
  try {
    const url = new URL(link);
    if (url.hostname === 'www.instagram.com') {
      url.hostname = 'ddinstagram.com';
      return url.toString();
    } else {
      return 'Это не ссылка на Instagram.';
    }
  } catch (error) {
    return 'Некорректная ссылка. Пожалуйста, отправьте правильную ссылку.';
  }
}

// Обработчик входящих обновлений
async function handleUpdate(update) {
  const message = update.message;

  if (message) {
    const chatId = message.chat.id;

    if (message.text === '/start' || message.text === '/help') {
      const welcomeMessage = 'Привет! Отправь мне ссылку на Instagram, и я преобразую её для тебя.';
      await sendTelegramMessage(chatId, welcomeMessage);
    } else if (message.text) {
      const userMessage = message.text;
      const transformedLink = transformInstagramLink(userMessage);
      await sendTelegramMessage(chatId, transformedLink);
    }
  }
}
