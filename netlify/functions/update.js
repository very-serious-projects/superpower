const axios = require("axios").default;

const TOKEN = "6273018871:AAG9usKB95FfCwDfMmzcAJZvi7h3ng4-y2E";
const chatID = "-1001917656900";
const API_URL = `https://api.telegram.org/bot${TOKEN}`;

exports.handler = async (event) => {
    try {
        console.log("Received an update from Telegram!", event.body);
        const body = JSON.parse(event.body);

        if (body.callback_query) {
            await callbackQueryHandler(body.callback_query)
        } else {
            const message = body.message ?? body.edited_message;

            if (message?.text?.includes('/roll')) {
                await processRoll();
            } else if (message?.text?.includes('/menu')) {
                await sendMenu();
            }
        }
    } catch (e) {
        await sendMessage('Ошибка, ты пидор')
        return;
    }

    return { statusCode: 200 };
};

const callbackQueryHandler = async (callbackQuery) => {

    const data = JSON.parse(callbackQuery.data); // Распарсить данные из callback_data


    if (data.action === "shuffle_abilities") {
        await processRoll();
        await axios.post(`${API_URL}/answerCallbackQuery`, {
            callback_query_id: callbackQuery.id,
            text: "Способности перемешаны!"
        });
    } else if (data.action === "call_me") {
        await editMessage(callbackQuery.message.message_id,`${callbackQuery.from.first_name}, ты пидор`)
    } else if (data.action === 'call_pidor') {
        await editMessage(callbackQuery.message.message_id,`${data.name}, ты пидор`)
    } else if (data.action === 'call_cool') {
        await editMessage(callbackQuery.message.message_id,`${data.name}, ты красава`)
    } else if (data.action === "call_someone") {
        const url = `${API_URL}/editMessageText`;
        return axios.get(url, {
            params: {
                chat_id: chatID,
                message_id: callbackQuery.message.message_id,
                text: "Выберите участника:",
                reply_markup: JSON.stringify({
                    inline_keyboard: await getKeyboardWithParticipants('call_pidor', 'call_cool')
                }),
            }
        }).then(response => response.data)
            .catch(error => console.error(error));
    }
    return { statusCode: 200 };
};

let abilities = [
    'Дополнительная хромосома',
    'Телепортация в Киргизию',
    'Елдаки вместо рук',
    'Молитва от выпадения прямой кишки',
    'Свап способностей',
    'Физкультурник и мяч',
    'Превращение в курьера Delivery Club',
    'Ты можешь запрещать всем срать, но сам обсираешься',
    'Ты оборотень, но превращаешься в чихуахуа',
    'У тебя каждый день спавнится дилдо в жопе',
    'Ты можешь становиться невидимым, но при этом на тебя каждый раз вешают кредит',
    'Ты превращаешься в вампира но пьешь мочу',
    'Ты можешь летать, но надо пожрать свое дерьмо',
    'У тебя ахуенная хата 500 квадратов бассейн и вся хуйня но ты живешь с мамкой Влада и теткой Илюхи',
    'Тебя хотят изнасиловать чурки, но ты обладатель пальца смерти, но только в попку',
    'Ты женат/вышла замуж',
    'Когда пьешь чай- ссышь водкой, но пить можно только с горла',
];

const shuffledArr = array => array.sort(() => 0.5 - Math.random());

async function getChatMembers() {
    // const response = await axios.get(`${API_URL}/getChatAdministrators?chat_id=${chatID}`);
    // return response.data.result.map(member => member.user.first_name);
    return [
        'Илья',
        'Денис',
        'Влад',
        'Яна',
        'Саня',
        'Маруся',
        'Рустик',
        'Вано',
        'Белый',
        'Хозяин Белого',
        'Хозяин Влада',
        'Даня',
    ]
}

async function sendMessage(string) {
    await axios.post(`${API_URL}/sendMessage`, {
        chat_id: chatID,
        text: string
    });
    console.log('Message sent');
}

function editMessage(messageId, newText) {
    const url = `${API_URL}/editMessageText`;

    return axios.get(url, {
        params: {
            chat_id: chatID,
            message_id: messageId,
            text: newText
        }
    }).then(response => response.data)
        .catch(error => console.error(error));
}


async function sendMenu() {
    await axios.post(`${API_URL}/sendMessage`, {
        chat_id: chatID,
        text: "Выберите действие:",
        reply_markup: {
            inline_keyboard: [
                [{ text: "Перемешать способности", callback_data: JSON.stringify({ action: 'shuffle_abilities' }) }],
                [{ text: "Хто Я?", callback_data: JSON.stringify({ action: 'call_me' }) }],
                [{ text: "Обозвать кого-то", callback_data: JSON.stringify({ action: 'call_someone' }) }]
            ]
        }
    });
}

async function processRoll() {
    console.log('Process Roll');
    const names = await getChatMembers();
    abilities = shuffledArr(abilities);

    let message = 'Новые способности на ' + new Date().toJSON().slice(0, 10).replace(/-/g, '/') + '\n';

    for (let index in names) {
        message += `${names[index]} - ${abilities[index % abilities.length]} \n`;
    }
    await sendMessage(message);
}

async function getKeyboardWithParticipants(action = 'call_pidor', my_action) {
    const res = [];
    const members = await getChatMembers();
    for (let name of members) {
        if (name === 'Денис') {
            res.push({
                    text: name,
                    callback_data: JSON.stringify({action: my_action ? my_action : action, name: name})
                }
            )
        } else {
            res.push({
                    text: name,
                    callback_data: JSON.stringify({action: action, name: name})
                }
            )
        }
        
    }
    // Разделяем массив res на подмассивы по 3 элемента
    const chunkedRes = [];
    for (let i = 0; i < res.length; i += 3) {
        chunkedRes.push(res.slice(i, i + 3));
    }

    return chunkedRes;
}

async function main() {


    const url = `${API_URL}/editMessageText`;

    return axios.get(url, {
        params: {
            chat_id: chatID,
            message_id: 3565,
            text: "Выберите участника:",
            reply_markup: JSON.stringify({
                inline_keyboard: await getKeyboardWithParticipants()
            }),
        }
    }).then(response => response.data)
        .catch(error => console.error(error));
    // console.log(await getKeyboardWithParticipants())
    // try {
    //     await axios.post(`${API_URL}/sendMessage`, {
    //         chat_id: chatID,
    //         text: "Выберите действие:",
    //         reply_markup: {
    //             inline_keyboard: await getKeyboardWithParticipants()
    //         }
    //     });
    // } catch (e) {
    //     console.log(e)
    // }
}

// main()

